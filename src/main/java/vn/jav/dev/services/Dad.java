package vn.jav.dev.services;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Uninterruptibles;

import vn.jav.dev.catalog.BET_TYPES;
import vn.jav.dev.catalog.SELECTION;
import vn.jav.dev.catalog.TYPE_OF_MATCH;
import vn.jav.dev.dto.BetDto;
import vn.jav.dev.util.DriverUtils;
import vn.jav.dev.util.JsoupUtils;

public class Dad extends Thread {

    private String userName;
    private String password;
    private String agent;
    private String target;
    private String childUserName;
    private String childPassword;
    private Integer bid;
    private boolean isBackwards;
    private ChromeDriver driver;
    private StringBuilder isBetted = new StringBuilder();

    /**
     * <pre>
     * isBackwards true: đánh theo
     * isBackwards fasle: đánh ngược
     * </pre>
     * @param userName
     * @param password
     * @param agent
     * @param target
     * @param childUserName
     * @param childPassword
     * @param bid
     * @param isBackwards
     */
    public Dad(String userName, String password, String agent, String target, String childUserName, String childPassword, Integer bid, boolean isBackwards) {
        this.userName = userName;
        this.password = password;
        this.agent = agent;
        this.target = target;
        this.childUserName = childUserName;
        this.childPassword = childPassword;
        this.bid = bid;
        this.isBackwards = isBackwards;
        this.driver = DriverUtils.initDriver();
    }

    private volatile boolean running = true;

    public void terminate() {
        running = false;
        driver.quit();
    }

    public void run() {
        Pair<String, String> of = login();
        if (of != null) {
            while (running) {
                try {
                    Thread.sleep(500);
                    refreshData(driver);
                    WebDriver rightFrame = driver.switchTo().frame("RightFrame");
                    Document document = Jsoup.parse(rightFrame.getPageSource());
                    List<BetDto> listA = getDataForBet(document);
                    if (!listA.isEmpty()) {
                        // chuyển qua con để đánh
                        driver.switchTo().window(of.getLeft());
                        DriverUtils.acceptAlert(driver);
                        driver.executeScript("$('span[id=luckyBetIntroModalClose]').click();");
                        Document cleanHtml = JsoupUtils.clean(driver);
                        for (BetDto betDto : listA) {
                            if (isBetted.toString().contains(betDto.matchId)) continue;
                            childBet(cleanHtml, betDto, driver);
                        }
                        // chuyển qua lại cha để đánh tiếp
                        driver.switchTo().window(of.getRight());
                        // xuất hiện pop up thông báo không còn trận nào thì bấm ok
                        DriverUtils.acceptAlert(driver);
                    }
                    driver.switchTo().defaultContent();
                } catch (InterruptedException e) {
                    running = false;
                } catch (Exception e) {
                }
            }
        }
    }

    /**
     * login to child and dad after return pair of child and dad tab
     * <pre>
     * Pair.getLeft() = childTab
     * Pair.getRight() = dadTab
     * </pre>
     */
    public Pair<String, String> login() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 2);
            driver.get(JsoupUtils.getLink("Bong88"));
            driver.executeScript("$('#en').click();");
            loginChild(wait, childUserName, childPassword);
            driver.executeScript("$('.login_btn').click();");
            if (!driver.findElements(By.className("tertiary")).isEmpty()) {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("tertiary"))).click();
            }
            Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
            driver.executeScript("window.open('about:blank', '_blank');");
            List<String> tabList = Lists.newArrayList(driver.getWindowHandles());
            driver.switchTo().window(tabList.get(1));
            driver.get(JsoupUtils.getLink("Agent B88"));
            loginDad(wait, userName, password);
            driver.findElementById("btnLogin").click();
            return Pair.of(tabList.get(0), tabList.get(1));
        } catch (Exception e) {
            return null;
        }
    }

    void childBet(Document html, BetDto bet, ChromeDriver driver) {
        switch (bet.typeOfBet) {
        case FULL_TIME:
        case FIRST_HALF:
            betForMatch(html, bet, driver);
        case NEXT_GOAL:
            break; // không đánh
        }
    }

    void betForMatch(Document html, BetDto bet, ChromeDriver driver) {
        switch (bet.typeOfMatch) {
        case ASIA_1X2:
            break; // full_time mới có đánh cái này
        case HDP:
            betHdp(html, bet, driver);
            break;
        case OU:
            betOu(html, bet, driver);
            break;
        case EURO_1X2:
            betEuro(html, bet, driver);
            break;
        }
    }

    void betEuro(Document html, BetDto bet, ChromeDriver driver) {
        DriverUtils.acceptAlert(driver);

        String home = bet.home;
        int halfOrAllMatch = bet.typeOfBet.getV();
        String _1x2 = bet.selection.getV().toLowerCase();
        String spanStr = "span:contains(" + home + ")";
        Elements _spans = html.select(spanStr);
        Element span = html.select(spanStr).first();
        if (_spans.size() > 1) {
            span = getSpainMain(_spans, home);
        }
        if (span != null) {
            Element tr = span.parent().parent();
            Elements spans = tr.select(".BBN.BRN.BLN > a[class*=odds-wrap] span[id*=':1:']");
            if (spans.size() == 0) {
                spans = tr.select(".BBN.BRN > a[class*=odds-wrap] span[id*=':1:']");
            }
            if (spans.size() == 0) {
                spans = tr.select(".BLN.BBN > a[class*=odds-wrap] span[id*=':1:']");
            }
            int size = spans.size();
            if (size != 0) {
                String id = "";
                if (size == 1) {
                    id = spans.get(0).attr("id");
                } else {
                    id = spans.get(halfOrAllMatch).attr("id");
                }
                String matchId = Splitter.on(":").splitToList(id).get(3); // bu:od:price:31658943:2:5.00:1
                String idClick = Joiner.on("").join("$('span[id*=\"", matchId, ":" + _1x2 + ":\"]').click();");
                driver.executeScript(idClick);
                DriverUtils.acceptAlert(driver);
                bet(driver, bet.moneyBid);
                DriverUtils.acceptAlert(driver);
                Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
            }
        }
        DriverUtils.acceptAlert(driver);
        isBetted.append(",").append(bet.matchId);
    }

    void betOu(Document html, BetDto bet, ChromeDriver driver) {
        DriverUtils.acceptAlert(driver);

        String home = bet.home;
        String homeOrAway = "";
        if (bet.selection.equals(SELECTION.OVER)) {
            homeOrAway = SELECTION.H.getV();
        } else {
            homeOrAway = SELECTION.A.getV();
        }
        int halfOrAllMatch = bet.typeOfBet.getV();
        String spanStr = "span:contains(" + home + ")";
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
        Elements _spans = html.select(spanStr);
        Element span = html.select(spanStr).first();
        if (_spans.size() > 1) {
            span = getSpainMain(_spans, home);
        }
        if (span != null) {
            Element tbodyFirst = span.parent().parent().parent();
            if (tbodyFirst != null) {
                Element tbodySecond = null;
                Element tbodyThird = null;
                if (tbodyFirst.nextElementSibling() != null){
                    boolean isExisted = tbodyFirst.nextElementSibling().cssSelector().contains("subrow");
                    if (isExisted) tbodySecond = tbodyFirst.nextElementSibling();
                }
                if (tbodySecond != null && tbodySecond.nextElementSibling() != null) {
                        boolean isExisted = tbodySecond.nextElementSibling().cssSelector().contains("subrow");
                        if (isExisted) tbodyThird = tbodySecond.nextElementSibling();
                }
                Elements spans = getSpansOu(tbodyFirst, tbodySecond, tbodyThird, bet.hdpPoint);
                int size = spans.size();
                if (size != 0) {
                    String id = "";
                    if (size == 1) {
                        id = spans.get(0).attr("id");
                    } else {
                        id = spans.get(halfOrAllMatch).attr("id");
                    }
                    String matchId = Splitter.on(":").splitToList(id).get(3);
                    String idClick = Joiner.on("").join("$('span[id*=\"", matchId, ":", homeOrAway, "\"]').click();");
                    driver.executeScript(idClick);
                    Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
                    DriverUtils.acceptAlert(driver);
                    bet(driver, bet.moneyBid);
                    DriverUtils.acceptAlert(driver);
                    Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
                }
            }
        }
        DriverUtils.acceptAlert(driver);
        isBetted.append(",").append(bet.matchId);
    }

    void betHdp(Document html, BetDto bet, ChromeDriver driver) {
        DriverUtils.acceptAlert(driver);
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
        DriverUtils.acceptAlert(driver);

        String home = bet.home;
        String homeOrAway = bet.selection.getV();
        int halfOrAllMatch = bet.typeOfBet.getV();
        String spanStr = "span:contains(" + home + ")";
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
        Elements _spans = html.select(spanStr);
        Element span = html.select(spanStr).first();
        if (_spans.size() > 1) {
            span = getSpainMain(_spans, home);
        }
        if (span != null) {
            Element tbodyFirst = span.parent().parent().parent();
            if (tbodyFirst != null) {
                Element tbodySecond = null;
                Element tbodyThird = null;
                if (tbodyFirst.nextElementSibling() != null) {
                    boolean isExisted = tbodyFirst.nextElementSibling().cssSelector().contains("subrow");
                    if (isExisted) tbodySecond = tbodyFirst.nextElementSibling();
                }
                if (tbodySecond != null && tbodySecond.nextElementSibling() != null) {
                        boolean isExisted = tbodySecond.nextElementSibling().cssSelector().contains("subrow");
                        if(isExisted) tbodyThird = tbodySecond.nextElementSibling();
                }
                Elements spans = getSpansHdp(tbodyFirst, tbodySecond, tbodyThird, bet.hdpPoint);
                int size = spans.size();
                if (size != 0) {
                    String id = "";
                    if (size == 1) {
                        id = spans.get(0).attr("id");
                    } else {
                        id = spans.get(halfOrAllMatch).attr("id");
                    }
                    String matchId = Splitter.on(":").splitToList(id).get(3);
                    String idClick = Joiner.on("").join("$('span[id*=\"", matchId, ":", homeOrAway, "\"]').click();");
                    driver.executeScript(idClick);
                    Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
                    DriverUtils.acceptAlert(driver);
                    bet(driver, bet.moneyBid);
                    DriverUtils.acceptAlert(driver);
                    Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
                }
            }
        }
        DriverUtils.acceptAlert(driver);
        isBetted.append(",").append(bet.matchId);
    }

    /* HTML example
        // Đánh HDP
         <td class="BBN BRN">
            <a class="odds-hdp odds-wrap ">
                <span class="hdp-point">1-1.5</span>
                <span class="odds-width  dummy-class">
                    <span class="&nbsp; dummy-more-class">
                        <span id="bu:od:price:34872933:h:-1.00:1" class="odds red">-1.00</span>
                    </span>
                </span>
            </a>
        </td>
        // Đánh OU
        <td class="BBN BRN BLN">
            <a class="odds-ou odds-wrap ">
                <span class="hdp-point">3-3.5</span>
                <span class="ou-tag">o</span>
                <span class="odds-width dummy-class">
                    <span class="&nbsp; dummy-more-class">
                        <span id="bu:od:price:34872935:h:0.90:1" class="odds black">0.90</span>
                    </span>
                </span>
            </a>
        </td>
    */
    /**
     * Mỗi trận đánh có tối đa 3 rows, dựa vào rate (ví dụ: 0-0.5 để biết đang đánh row nào)
     *
     * @param tbody
     * @param tbodyNext
     * @param tbodyThree
     * @param rate
     * @return
     */
    Elements getSpansHdp(Element tbody, Element tbodyNext, Element tbodyThree, String rate) {
        Elements spans = tbody.select(Joiner.on("").join(".BBN.BRN > a[class*=odds-hdp] > span[class*=hdp-point]:contains(" , rate , ") + span span[id^=bu]"));
        if (spans.size() != 0)
            return spans;
        if (tbodyNext != null) {
            spans = tbodyNext.select(Joiner.on("").join(".BBN.BRN > a[class*=odds-hdp] > span[class*=hdp-point]:contains(" , rate , ") + span span[id^=bu]"));
            if (spans.size() != 0)
                return spans;
        }
        if (tbodyThree != null) {
            spans = tbodyThree.select(Joiner.on("").join(".BBN.BRN > a[class*=odds-hdp] > span[class*=hdp-point]:contains(" , rate , ") + span span[id^=bu]"));
            if (spans.size() != 0)
                return spans;
        }
        spans = tbody.child(0).select(".BBN.BRN > a[class*=odds-hdp] span[id^=bu]");
        return spans;
    }

    Elements getSpansOu(Element tbody, Element tbodyNext, Element tbodyThree, String rate) {
        Elements spans = tbody.select(Joiner.on("").join(".BBN.BRN.BLN > a[class*=odds-ou] > span[class*=hdp-point]:contains(" , rate , ") + span + span span[id^=bu]"));
        if (spans.size() != 0)
            return spans;
        if (tbodyNext != null) {
            spans = tbodyNext.select(Joiner.on("").join(".BBN.BRN.BLN > a[class*=odds-ou] > span[class*=hdp-point]:contains(" , rate , ") + span + span span[id^=bu]"));
            if (spans.size() != 0)
                return spans;
        }
        if (tbodyThree != null) {
            spans = tbodyThree.select(Joiner.on("").join(".BBN.BRN.BLN > a[class*=odds-ou] > span[class*=hdp-point]:contains(" , rate , ") + span + span span[id^=bu]"));
            if (spans.size() != 0)
                return spans;
        }
        spans = tbody.child(0).select(".BBN.BRN.BLN > a[class*=odds-ou] span[id^=bu]");
        return spans;
    }

    private Element getSpainMain(Elements spans, String home){
        for(Element span : spans){
            String text = span.text().trim();
            if(text.equals(home) && (span.cssSelector().contains("Red") || span.cssSelector().contains("Blue"))){
                return span;
            }
        }
        return spans.get(0);
    }

    void bet(ChromeDriver driver, Double bid) {
        DriverUtils.acceptAlert(driver);
        // Add money bet
        String sendMoneyBet = "$('.stake-input').val('" + bid + "');";
        driver.executeScript(sendMoneyBet);
        driver.executeScript("$('input[id*=acceptanyodds]').click();");// Accept any odds
        Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
        DriverUtils.acceptAlert(driver);
        driver.executeScript("$('input[id*=submittc]').click();");
        DriverUtils.acceptAlert(driver);
        Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
        if (DriverUtils.isAlertPresent(driver)) {
            if (driver.switchTo().alert().getText().contains("Your stake cannot be lower than the minimum bet.")) {
                DriverUtils.acceptAlert(driver);
                Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
                driver.executeScript("$('input[id*=submittc]').click();");
                DriverUtils.acceptAlert(driver);
            } else {
                DriverUtils.acceptAlert(driver);
                Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
                DriverUtils.acceptAlert(driver);
                Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
                DriverUtils.acceptAlert(driver);
                Uninterruptibles.sleepUninterruptibly(1, TimeUnit.SECONDS);
                DriverUtils.acceptAlert(driver);
            }
        }
    }

    /**
     * Lần đầu tiên load lên nếu tồn tại trận đánh trước đó của thằng con thì bỏ qua
     */
    private boolean isBetExisted = true;// true lần đầu tiên, false từ lần thứ 2 trở đi

    private List<String> notBetList = Lists.newArrayList(
            "total corners",
            "oe",
            "total goal",
            "double chance",
            "correct score",
            "fh correct score",
            "team to kick-off",
            "ht/ft",
            "fg/lg",
            "mix parlay");
    List<BetDto> getDataForBet(Document document) throws Exception {
        List<BetDto> betList = Lists.newArrayList();
        Elements trs = document.select("#divDataList > table > tbody > tr");
        for (Element tr : trs) {
            String mathId = tr.select("td.TAC > span.DeepBlue.FW700").first().text().replace("i", "").trim();
            if (isBetExisted) {
                isBetted.append(",").append(mathId);
                continue;
            }
            // chỉ lấy ra các trận đang chạy và chưa đánh
            if (tr.select("td.TAC").get(3).text().trim().equals("Running") && !isBetted.toString().contains(mathId)) {
                Element td = tr.select("td.TAR.VAT").first();
                String tdText = td.text().toLowerCase();
                // Các trường hợp không đánh:
                if(notBetList.stream().filter(str -> tdText.contains(str)).findFirst().isPresent()) continue;
                String str = td.select(".WSN > .FSI").first().text();
                BET_TYPES typeOfBet = typeOfBet(str);
                if (typeOfBet.equals(BET_TYPES.NEXT_GOAL)) continue;
                Elements spans = td.getElementsByTag("span");
                String select = spans.first().text();
                String home = spans.get(6).text();
                String away = spans.get(7).text();
                TYPE_OF_MATCH typeOfMatch = typeOfMatch(str);
                if (!Optional.ofNullable(typeOfMatch).isPresent()) continue;
                BetDto child = new BetDto();
                // Đánh ngược
                if (isBackwards) {
                    child.home = away;
                    child.away = home;
                    if (select.trim().equals(SELECTION.HOME.getV())) {
                        select = SELECTION.AWAY.getV();
                    } else if (select.trim().equals(SELECTION.AWAY.getV())) {
                        select = SELECTION.HOME.getV();
                    } else if (select.trim().equals(SELECTION.UNDER.getV())) {
                        select = SELECTION.OVER.getV();
                    } else if (select.trim().equals(SELECTION.OVER.getV())) {
                        select = SELECTION.UNDER.getV();
                    }
                // Đánh theo
                } else {
                    child.home = home;
                    child.away = away;
                }
                child.typeOfBet = typeOfBet;
                child.typeOfMatch = typeOfMatch;
                child.selection = selection(typeOfMatch, select, child.home, child.away);
                child.league = td.select(".CF60").first().text().trim();
                // id của trận đấu
                child.matchId = mathId;
                child.hdpPoint = DriverUtils.hdpPoint(spans.select(".DeepBlue.FW700").text());
                // thời gian đánh chính xác để so sánh
                child.betTime = StringUtils.substringAfter(tr.select("td.TAC").get(1).text(), "Football ");
                Elements tds = tr.select("td.TAR");
                if (tds.size() > 1) {
                    // số tiền đánh = số tiền đánh tại con nhân với bid
                    child.moneyBid = (bid * NumberUtils.toDouble(tds.get(1).text().trim().split("\\s+")[0]));
                }  else {
                    child.moneyBid = (10.0); // mac dinh danh 10 dong
                }
                betList.add(child);
            }
        }
        isBetExisted = false;
        return betList;
    }

    public BET_TYPES typeOfBet(String str) {
        if (str.contains("First Half")) {
            return BET_TYPES.FIRST_HALF;
        }
        if (str.contains("Next Goal")) {
            return BET_TYPES.NEXT_GOAL;
        }
        return BET_TYPES.FULL_TIME;
    }

    // only apply for FULL_TIME and HALF_TIME
    private TYPE_OF_MATCH typeOfMatch(String str) {
        if (str.toLowerCase().contains("first half hdp")) {
            return TYPE_OF_MATCH.HDP;
        }
        if (str.toLowerCase().contains("first half o/u")) {
            return TYPE_OF_MATCH.OU;
        }
        if (str.toLowerCase().contains("first half 1x2")) {
            return TYPE_OF_MATCH.EURO_1X2;
        }
        return TYPE_OF_MATCH.fromV(str);
    }

    // only apply for FULL_TIME and HALF_TIME
    public SELECTION selection(TYPE_OF_MATCH typeOfMatch, String select, String home, String away) {
        switch (typeOfMatch) {
        case OU:
        case EURO_1X2:
            return SELECTION.fromV(select);
        case ASIA_1X2:
        case HDP:
            if (select.contains(home)) {
                return SELECTION.H;
            }
            if (select.contains(away)) {
                return SELECTION.A;
            }
        }
        return null;
    }

    public void refreshData(ChromeDriver driver) {
        while (true) {
            // bấm vào 1.17
            driver.executeScript("document.getElementById('MenuFrame').contentWindow.document.getElementById('TotalBet2_Agent_Member').onclick();");
            // nếu alert xuất hiện thông báo không có trận nào
            if (DriverUtils.isAlertPresent(driver)) {
                driver.switchTo().alert().accept();
            // nếu có trận thì xem có phải là agent cần đánh hay không
            } else {
                WebDriver rightFrame = driver.switchTo().frame("RightFrame");
                // tồn tại agent cần click thì click vào để chọn đến target
                if(!rightFrame.findElements(By.linkText(agent)).isEmpty()) {
                    // click để truy cập vào target
                    rightFrame.findElement(By.linkText(agent)).click();
                    // kiểm tra xem có target không
                    if(!rightFrame.findElements(By.linkText(target)).isEmpty()) {
                        // nếu có thì bấm vào đó để xem các trận đánh của target
                        rightFrame.findElement(By.linkText(target)).click();
                        driver.switchTo().defaultContent();
                        break;
                    }
                }
                // không có thì trở lại ban đầu và tiếp tục bấm 1.17
                driver.switchTo().defaultContent();
                Uninterruptibles.sleepUninterruptibly(RandomUtils.nextInt(3, 6), TimeUnit.SECONDS);
            }
        }
    }

    void loginDad(WebDriverWait wait, String userName, String passWord) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtUserName"))).sendKeys(userName);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtPassWord"))).sendKeys(passWord);
    }

    void loginChild(WebDriverWait wait, String userName, String passWord) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtID"))).sendKeys(userName);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtPW"))).sendKeys(passWord);
    }
}
