package vn.jav.dev.controller;

import java.net.URL;
import java.time.LocalDate;
import java.time.Month;
import java.util.ResourceBundle;

import org.apache.commons.lang3.math.NumberUtils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import vn.jav.dev.services.Dad;
import vn.jav.dev.util.FxUtils;

public class Controller implements Initializable {

    @FXML
    private Button startBtn;
    @FXML
    private Button stopBtn;
    @FXML
    private TextField txtUserName;
    @FXML
    private TextField txtPassword;
    @FXML
    private TextField txtAgent;
    @FXML
    private TextField txtChidUserNameFollow;
    @FXML
    private TextField txtChidUserName;
    @FXML
    private TextField txtChildPassword;
    @FXML
    private TextField txtBid;
    @FXML
    private CheckBox isBackwards;
    private Thread thread = null;
    private Dad dad;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        stopBtn.setDisable(true);
    }

    public void startSelenium(ActionEvent event) {
        try {
            if (LocalDate.now().compareTo(LocalDate.of(2017, Month.NOVEMBER, 30)) > 0) {
                FxUtils.expired();
            } else {
                startBtn.setText("RUNNING");
                disable(true);
                stopBtn.setDisable(false);
                dad = new Dad(
                        txtUserName.getText().trim(),
                        txtPassword.getText().trim(),
                        txtAgent.getText().trim(),
                        txtChidUserNameFollow.getText().trim(),
                        txtChidUserName.getText().trim(),
                        txtChildPassword.getText().trim(),
                        NumberUtils.toInt(txtBid.getText().trim(), 1),
                        isBackwards.isSelected() ? false : true);
                thread = new Thread(dad);
                thread.start();
            }
        } catch (Exception e) {
            FxUtils.exceptionDialog(e);
            disable(false);
            stopBtn.setDisable(true);
            startBtn.setText("START");
        }
    }

    public void stopSelenium(ActionEvent event) {
        try {
            if (FxUtils.confirmationDialog(startBtn)) {
                if (thread != null) {
                    stopBtn.setDisable(true);
                    disable(false);
                    isBackwards.setSelected(false);
                    dad.terminate();
                    thread.join();
                }
            }
        } catch (Exception e) {
            FxUtils.exceptionDialog(e);
        }
    }

    void disable(boolean b) {
        startBtn.setDisable(b);
        txtUserName.setDisable(b);
        txtPassword.setDisable(b);
        txtAgent.setDisable(b);
        txtChidUserNameFollow.setDisable(b);
        txtChidUserName.setDisable(b);
        txtChildPassword.setDisable(b);
        isBackwards.setDisable(b);
        txtBid.setDisable(b);
    }
}
