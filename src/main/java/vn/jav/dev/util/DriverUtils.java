package vn.jav.dev.util;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.util.concurrent.Uninterruptibles;

import vn.jav.dev.catalog.CONFIG;

public class DriverUtils {

    public static ChromeDriver initDriver() {
        System.setProperty("webdriver.chrome.driver", CONFIG.CHROME_DRIVER_PATH);
        ChromeOptions options = new ChromeOptions();
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        // disable auto save
        chromePrefs.put("credentials_enable_service", false);
        chromePrefs.put("profile.password_manager_enabled", false);
        options.setExperimentalOption("prefs", chromePrefs);
        // maximum win down
        options.addArguments("--start-maximized");
        // private mode
        options.addArguments("--incognito");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        // disable chrome is being controlled by ....
        options.addArguments("disable-infobars");
        return new ChromeDriver(options);
    }

    public static void childLogin(ChromeDriver driver, String username, String password) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver, 2);
        driver.get(JsoupUtils.getLink("Sbo"));
        login(wait, username, password);
        driver.executeScript("$('.sign-in').click();");
        driver.executeScript("$('#lang-menu > li > a:contains(English)').click();");
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
    }

    static void login(WebDriverWait wait, String username, String password) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username"))).sendKeys(username);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password"))).sendKeys(password);
    }

    public static boolean isAlertPresent(ChromeDriver driver) {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoSuchSessionException e) {
            return false;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    public static void acceptAlert(ChromeDriver driver) {
        if (isAlertPresent(driver)) {
            driver.switchTo().alert().accept();
        }
    }

    public static String hdpPoint(String str) {
        str = str.trim();
        switch (str) {
        // 0
        case "0.00":
        case "0":
            return "0.0";
        case "0.25":
        case "-0.25":
            return "0-0.5";
        case "0.5":
        case "-0.5":
        case "0.50":
        case "-0.50":
            return "0.50";
        case "0.75":
        case "-0.75":
            return "0.5-1";
        // 1
        case "1.00":
        case "-1.00":
        case "1":
        case "-1":
            return "1.0";
        case "1.25":
        case "-1.25":
            return "1-1.5";
        case "1.5":
        case "-1.5":
        case "1.50":
        case "-1.50":
            return "1.50";
        case "1.75":
        case "-1.75":
            return "1.5-2";
        // 2
        case "2":
        case "-2":
        case "2.00":
        case "-2.00":
            return "2.0";
        case "2.25":
        case "-2.25":
            return "2-2.5";
        case "2.5":
        case "-2.5":
        case "2.50":
        case "-2.50":
            return "2.50";
        case "2.75":
        case "-2.75":
            return "2.5-3";
        // 3
        case "3":
        case "-3":
        case "3.00":
        case "-3.00":
            return "3.0";
        case "3.25":
        case "-3.25":
            return "3-3.5";
        case "3.5":
        case "-3.5":
        case "3.50":
        case "-3.50":
            return "3.50";
        case "3.75":
        case "-3.75":
            return "3.5-4";
        // 4
        case "4":
        case "-4":
        case "4.00":
        case "-4.00":
            return "4.0";
        case "4.25":
        case "-4.25":
            return "4-4.5";
        case "4.5":
        case "-4.5":
        case "4.50":
        case "-4.50":
            return "4.50";
        case "4.75":
        case "-4.75":
            return "4.5-5";
        // 5
        case "5":
        case "-5":
        case "5.00":
        case "-5.00":
            return "5.0";
        case "5.25":
        case "-5.25":
            return "5-5.5";
        case "5.5":
        case "-5.5":
        case "5.50":
        case "-5.50":
            return "5.50";
        case "5.75":
        case "-5.75":
            return "5.5-6";
        // 6
        case "6":
        case "-6":
        case "6.00":
        case "-6.00":
            return "6.0";
        case "6.25":
        case "-6.25":
            return "6-6.5";
        case "6.5":
        case "-6.5":
        case "6.50":
        case "-6.50":
            return "6.50";
        case "6.75":
        case "-6.75":
            return "6.5-7";
        // 7
        case "7":
        case "-7":
        case "7.00":
        case "-7.00":
            return "7.0";
        case "7.25":
        case "-7.25":
            return "7-7.5";
        case "7.5":
        case "-7.5":
        case "7.50":
        case "-7.50":
            return "7.50";
        case "7.75":
        case "-7.75":
            return "7.5-8";
        // 8
        case "8":
        case "-8":
        case "8.00":
        case "-8.00":
            return "8.0";
        case "8.25":
        case "-8.25":
            return "8-8.5";
        case "8.5":
        case "-8.5":
        case "8.50":
        case "-8.50":
            return "8.50";
        case "8.75":
        case "-8.75":
            return "8.5-9";
        // 9
        case "9":
        case "-9":
        case "9.00":
        case "-9.00":
            return "9.0";
        case "9.25":
        case "-9.25":
            return "9-9.5";
        case "9.5":
        case "-9.5":
        case "9.50":
        case "-9.50":
            return "9.50";
        case "9.75":
        case "-9.75":
            return "9.5-10";
        // 10
        case "10":
        case "-10":
        case "10.00":
        case "-10.00":
            return "10.0";
        case "10.25":
        case "-10.25":
            return "10-10.5";
        case "10.5":
        case "-10.5":
        case "10.50":
        case "-10.50":
            return "10.50";
        case "10.75":
        case "-10.75":
            return "10.5-11";
         // 11
        case "11":
        case "-11":
        case "11.00":
        case "-11.00":
            return "11.0";
        case "11.25":
        case "-11.25":
            return "11-11.5";
        case "11.5":
        case "-11.5":
        case "11.50":
        case "-11.50":
            return "11.50";
        case "11.75":
        case "-11.75":
            return "11.5-12";
         // 12
        case "12":
        case "-12":
        case "12.00":
        case "-12.00":
            return "12.0";
        case "12.25":
        case "-12.25":
            return "12-12.5";
        case "12.5":
        case "-12.5":
        case "12.50":
        case "-12.50":
            return "12.50";
        case "12.75":
        case "-12.75":
            return "12.5-13";
         // 13
        case "13":
        case "-13":
        case "13.00":
        case "-13.00":
            return "13.0";
        case "13.25":
        case "-13.25":
            return "13-13.5";
        case "13.5":
        case "-13.5":
        case "13.50":
        case "-13.50":
            return "13.50";
        case "13.75":
        case "-13.75":
            return "13.5-14";
         // 14
        case "14":
        case "-14":
        case "14.00":
        case "-14.00":
            return "14.0";
        case "14.25":
        case "-14.25":
            return "14-14.5";
        case "14.5":
        case "-14.5":
        case "14.50":
        case "-14.50":
            return "14.50";
        case "14.75":
        case "-14.75":
            return "14.5-15";
         // 15
        case "15":
        case "-15":
        case "15.00":
        case "-15.00":
            return "15.0";
        case "15.25":
        case "-15.25":
            return "15-15.5";
        case "15.5":
        case "-15.5":
        case "15.50":
        case "-15.50":
            return "15.50";
        case "15.75":
        case "-15.75":
            return "15.5-16";
         // 16
        case "16":
        case "-16":
        case "16.00":
        case "-16.00":
            return "16.0";
        case "16.25":
        case "-16.25":
            return "16-16.5";
        case "16.5":
        case "-16.5":
        case "16.50":
        case "-16.50":
            return "16.50";
        case "16.75":
        case "-16.75":
            return "16.5-17";
        default:
            return str;
        }
    }
}
