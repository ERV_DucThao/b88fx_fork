package vn.jav.dev.catalog;

import java.util.HashMap;
import java.util.Map;

public enum BET_TYPES {

    FULL_TIME(0), // Full Time
    FIRST_HALF(1), // First Half
    NEXT_GOAL(2), // Next Goal
    ;

    private BET_TYPES(Integer v) {
        this.v = v;
    }

    private static Map<Integer, BET_TYPES> vMap = new HashMap<Integer, BET_TYPES>();
    static {
        for (BET_TYPES r : BET_TYPES.values()) {
            vMap.put(r.v, r);
        }
    }

    public static BET_TYPES fromV(Integer v) {
        return vMap.get(v);
    }

    public Integer v;

    public Integer getV() {
        return this.v;
    }

}
