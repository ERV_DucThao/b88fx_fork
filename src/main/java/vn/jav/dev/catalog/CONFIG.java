package vn.jav.dev.catalog;

import java.util.ResourceBundle;

public class CONFIG {

    public static final String CHROME_DRIVER_PATH;

    static {
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        CHROME_DRIVER_PATH = bundle.getString("CHROME_DRIVER_PATH");
    }

}
